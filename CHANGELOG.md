# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.3.2] - 2022-10-11

### Added

- New component added to export orders in CSV format. Allows users to download orders. 

## [1.3.1] - 2022-10-08

### Fixed

- Bug [#7](https://gitlab.com/pitmutt/zgo/-/issues/7) for saving a viewing key.

## [1.3.0] - 2022-10-01

### Added

- Added new connection for Xero account code
- Added new service for Xero integration

### Changed

- Login updated to price sessions in USD and include the Pro service.
- Settings component updated for compatibility with Android devices
- Settings component updated to use observable when saving Account Code
- xeroService's saveAccountCode function optimized to export observable 
- Field for Xero's AccountCode added to Settings component's integration tab
- Listorders component updated to show date in ANSI international format.
- Settings component updated to use owner's invoices field to control 
  integrations tab (Pro version)
- Orders list updated to show payment confirmation only when service is
  activated and a viewing key exists.
- Updated Order and Owner model to include new Xero integration fields

## [1.2.2] - 2022-08-05

### Added

- Convenience buttons on checkout for wallets that are not ZIP-321-compliant
- PmtService Component first alpha version ready for testing

### Fixed

- Memo for checkout orders

## [1.2.1] - 2022-08-01

### Changed

- Improved formatting of dialogs

## [1.2.0] - 2022-07-27

### Added

- Dialogs created for viewing invoices and receipts URLs
- Notifier service created - replaces snackbar used for reporting invalid viewing key.
- Notifier component created - used to apply custom style to message sent by Notifier service.
- Notification to user when the submitted viewing key is not valid


### Changed

- Item dialog look and feel reviewed and updated
- Order list look and feel reviewed and updated
- Available Items list look and feel reviewed and updated
- Set configuration global parameters in one place
- Receipt look and feel reviewed and updated
- Invoice look and feel reviewed and updated
- Checkout dialog with QR code reviewed and updated
- Invoice dialog completed. Notifications to inform successful/failed URL-copy-to-clipboard added. 
- Obsolete function `updateOwner` removed
- Buttons in main shop page have been enclosed to display inside a fixed area.
- Shop page buttons redesigned
- Settings form redesigned
- Order list look and feel reviewed and updated - some unused CSS classes removed.
- Added viewing key field to owner model
- Paid status icons added to order's title
- Order detail redesigned 

### Fixed

- Fixed order memo for checkout
- Fixed display of amounts in item list when using *zatoshis*
- Fixed sorting of items in list
- Fixed sorting of orders in list

## [1.1.2] - 2022-05-24

### Added

- Button in QR code prompt to copy ZGo address.
- Button in QR code prompt to copy transaction amount.
- Button in QR code prompt to copy transaction memo.

## [1.1.1] - 2022-05-24

### Added

- Invoice display via URL
- Invoice button for closing order
- Field `paid` for Order type

## [1.1.0] - 2022-05-20

### Changed

- Services interacting with backend API were modified to support the new [ZGo Backend](https://gitlabl.com/pitmutt/zgo-backend).

### Removed

- NodeJS back-end


