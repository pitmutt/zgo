import {Injectable} from '@angular/core';
import {Subject, Subscription, BehaviorSubject, Observable} from 'rxjs';
import {HttpClient, HttpParams, HttpHeaders} from '@angular/common/http';
import {UserService} from './user.service';
import { Owner } from './owner.model';

import { ConfigData } from './configdata';

var Buffer = require('buffer/').Buffer;
//import {User} from './user.model';

@Injectable({providedIn: 'root'})
export class FullnodeService{
	beUrl = ConfigData.Be_URL;
	private dataStore: { height: number, memoList: string[], addr: string, price: number } = { height: 0, memoList: [], addr: '', price:0 };
	private _heightUpdated: BehaviorSubject<number> = new BehaviorSubject(this.dataStore.height);
	private _memoUpdated: BehaviorSubject<string[]> = new BehaviorSubject(this.dataStore.memoList);
	private _addrUpdated: BehaviorSubject<string> = new BehaviorSubject(this.dataStore.addr);
	private _priceUpdated: BehaviorSubject<number> = new BehaviorSubject(this.dataStore.price);
	public readonly addrUpdate: Observable<string> = this._addrUpdated.asObservable();
	public readonly heightUpdate: Observable<number> = this._heightUpdated.asObservable();
	public readonly memoUpdate: Observable<string[]> = this._memoUpdated.asObservable();
	public readonly priceUpdate: Observable<number> = this._priceUpdated.asObservable();
	public readonly ownerUpdate: Observable<Owner>;
	private UserSub: Subscription = new Subscription();
	private reqHeaders: HttpHeaders;
	private owner: Owner = {
		_id: '',
		name: '',
		address: '',
		currency: 'usd',
		tax: false,
		taxValue: 0,
		vat: false,
		vatValue: 0,
		first: '',
		last: '',
		email: '',
		street: '',
		city: '',
		state: '',
		postal: '',
		phone: '',
		paid: false,
		website: '',
		country: '',
		zats: false,
		invoices: false,
		expiration: new Date(Date.now()).toISOString(),
		payconf: false,
		viewkey: '',
		crmToken: ''
	};

	constructor(private http: HttpClient, public userService: UserService){
		var auth = 'Basic ' + Buffer.from(ConfigData.UsrPwd).toString('base64');
        console.log('auth: ' + auth);
		this.reqHeaders = new HttpHeaders().set('Authorization', auth);
		console.log(this.reqHeaders);
		this.ownerUpdate = userService.ownerUpdate;
		this.getAddr();
		this.getHeight();
		this.ownerUpdate.subscribe((owner) => {
			this.owner = owner;
			this.getPrice(this.owner.currency);
		});
	}

	getHeight(){
		let obs = this.http.get<{message: string, height: number}>(this.beUrl+'api/blockheight', { headers: this.reqHeaders });
		obs.subscribe((BlockData) => {
			this.dataStore.height = BlockData.height;
			this._heightUpdated.next(Object.assign({}, this.dataStore).height);
		});

		return obs;
	}

	getPrice(currency: string){
		//var currency = 'usd';
		const params = new HttpParams().append('currency', currency);
		let obs = this.http.get<{message: string, price: any}>(this.beUrl+'api/price', { headers:this.reqHeaders, params: params, observe: 'response'});
		obs.subscribe((PriceData) => {
			if (PriceData.status == 200) {
				this.dataStore.price = PriceData.body!.price.price;
				console.log("price", this.dataStore.price);
				this._priceUpdated.next(Object.assign({},this.dataStore).price);
			} else {
				console.log('No price found for currency', currency);
			}
		});

		return obs;
	}

	getAddr() {
		let obs = this.http.get<{message: string, addr: string}>(this.beUrl+'api/getaddr', { headers: this.reqHeaders });

		obs.subscribe((AddrData) => {
			this.dataStore.addr = AddrData.addr;
			this._addrUpdated.next(Object.assign({}, this.dataStore).addr);
		});

		return obs;
	}
}
