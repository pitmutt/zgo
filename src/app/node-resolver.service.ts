import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { FullnodeService} from './fullnode.service';

@Injectable({
	providedIn: 'root'
})

export class NodeResolverService implements Resolve<any> {
	constructor(private fullnode: FullnodeService) {}

	resolve(route: ActivatedRouteSnapshot): Observable<any> {
		console.log('Called getAddr in resolver...', route);
		return this.fullnode.getAddr().pipe(
			catchError(error => {
				return of('No data');
			})
		);
	}
}
