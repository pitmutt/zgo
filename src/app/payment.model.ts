export interface Payment {
	_id?: string;
	address: string;
	blocktime: number;
	expiration: string;
	amount: number;
}
