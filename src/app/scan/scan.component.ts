import { Inject, Component, OnInit, ViewEncapsulation} from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';

import { NotifierService } from '../notifier.service';

var QRCode = require('easyqrcodejs');
var URLSafeBase64 = require('urlsafe-base64');
var Buffer = require('buffer/').Buffer;

@Component({
	selector: 'app-scan',
	templateUrl: './scan.component.html',
	styleUrls: ['./scan.component.css']
})

export class ScanComponent implements OnInit{
	address: string;
	total: number;
	session: string;
	codeString: string = '';
	pay: boolean = false;
	zcashUrl: SafeUrl;
	
	constructor(
		private dialogRef: MatDialogRef<ScanComponent>,
		private sanitizer: DomSanitizer,
		@Inject(MAT_DIALOG_DATA) public data: { totalZec: number, addr: string, session: string, pay: boolean},
		private notifierService : NotifierService ) {
	
		this.address = data.addr;
		this.total = data.totalZec;
		this.session = data.session;
		this.pay = data.pay;
		if (this.pay) {
			this.codeString = `zcash:${this.address}?amount=${this.total.toFixed(8)}&memo=${URLSafeBase64.encode(Buffer.from('ZGOp::'.concat(this.session)))}`; 
		} else {
			this.codeString = `zcash:${this.address}?amount=${this.total.toFixed(8)}&memo=${URLSafeBase64.encode(Buffer.from('ZGO::'.concat(this.session)))}`; 
		}
		this.zcashUrl = this.sanitizer.bypassSecurityTrustUrl(this.codeString);
	}

	ngOnInit() {
		var qrcode = new QRCode(document.getElementById("checkout-qr"), {
			text: this.codeString,
			logo: "/assets/zcash.png",
			logoWidth: 80,
			logoHeight: 80
		});
	}

	confirm() {
		this.dialogRef.close(true);
	}

	close() {
		this.dialogRef.close(false);
	}

	copyAddress() {
		if (!navigator.clipboard) {
			alert("Copy functionality not supported");
			this.notifierService
			    .showNotification("Copy functionality not supported","Close","error");
		}
		try {
			navigator.clipboard.writeText(this.address);
		} catch (err) {
			console.error("Error", err);
		}
	}

	copyAmount() {
		if (!navigator.clipboard) {
//			alert("Copy functionality not supported");
			this.notifierService
			    .showNotification("Copy functionality not supported","Close","error");
		}
		try {
			navigator.clipboard.writeText(this.total.toString());
		} catch (err) {
			this.notifierService
			    .showNotification("Error while copying ammount","Close","error");
//			console.error("Error", err);
		}
	}

	copyMemo() {
		if (!navigator.clipboard) {
//			alert("Copy functionality not supported");
			this.notifierService
			    .showNotification("Copy functionality not supported","Close","error");
		}
		try {
			if (this.pay) {
				navigator.clipboard.writeText("ZGOp::" + this.session);
			} else {
				navigator.clipboard.writeText("ZGO::" + this.session + " Reply-To:<your z-addr>");
			}
		} catch (err) {

			this.notifierService
			    .showNotification("Error while verifying payment","Close","error");
//			console.error("Error", err);
		}
	}
}
