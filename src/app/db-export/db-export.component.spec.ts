import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DbExportComponent } from './db-export.component';

describe('DbExportComponent', () => {
  let component: DbExportComponent;
  let fixture: ComponentFixture<DbExportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DbExportComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DbExportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
