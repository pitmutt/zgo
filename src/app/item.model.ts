export interface Item {
	_id?: any;
	name: string;
	description: string;
	owner: string;
	cost: number;
}
