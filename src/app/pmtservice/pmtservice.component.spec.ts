import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PmtserviceComponent } from './pmtservice.component';

describe('PmtserviceComponent', () => {
  let component: PmtserviceComponent;
  let fixture: ComponentFixture<PmtserviceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PmtserviceComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PmtserviceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
