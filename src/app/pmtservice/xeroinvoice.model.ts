export interface XeroInvoice {
    inv_Type : string;
    inv_Id : string;
    inv_No : string;
    inv_Contact : string; 
    inv_Currency : string;
    inv_CurrencyRate : number;
    inv_Total : number;
    inv_Status : string;
    inv_Date : Date;
    inv_shortCode : string;
    inv_ProcDate : Date;
}