export interface PmtData {
	ownerId: string;
	invoice: string;
	amount: number;
	currency: string;
	shortcode: string;
}
