import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { NotifierComponent } from './notifier/notifier.component';

@Injectable({
  providedIn: 'root'
})
export class NotifierService {

  constructor(private snackBar:MatSnackBar) { }

  showNotification(displayMessage:string, buttonText: string, messageType: 'error' | 'success' | 'warning') {
    this.snackBar.openFromComponent(NotifierComponent, {
      data: {
        message: displayMessage,
        buttonText: buttonText,
        type : messageType
      },  
      duration: 4000,
      verticalPosition: 'top',
      panelClass: [messageType] 
    });
    this.playSound();
  }

  playSound() {
//    console.log('Play sound called...');
    let audio = new Audio();
    audio.src = '../assets/notifier_1.mp3';
    audio.load();
    audio.play();
  }

}
