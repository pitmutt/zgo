import { Inject, Component, OnInit, ViewEncapsulation} from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';

var QRCode = require('easyqrcodejs');

@Component({
  selector: 'app-receipt-qr',
  templateUrl: './receipt-qr.component.html',
  styleUrls: ['./receipt-qr.component.css']
})
export class ReceiptQRComponent implements OnInit {
	receiptUrl: SafeUrl;
	codeString: string = '';

	constructor(
		private dialogRef: MatDialogRef<ReceiptQRComponent>,
		private sanitizer: DomSanitizer,
		@Inject(MAT_DIALOG_DATA) public data: { order: string}
	) {
		this.codeString = `https://zgo.cash/receipt/${data.order}`;
		this.receiptUrl = this.sanitizer.bypassSecurityTrustUrl(this.codeString);
	}

  ngOnInit(): void {
		var qrcode = new QRCode(document.getElementById("receipt-qr"), {
			text: this.codeString,
			logo: "/assets/zgo-prp.png",
			logoWidth: 80,
			logoHeight: 80
		});
  
  }

  close() {
	  this.dialogRef.close(true);
  }
}
