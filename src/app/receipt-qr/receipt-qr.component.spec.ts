import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReceiptQRComponent } from './receipt-qr.component';

describe('ReceiptQRComponent', () => {
  let component: ReceiptQRComponent;
  let fixture: ComponentFixture<ReceiptQRComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReceiptQRComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReceiptQRComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
