export interface Owner {
	_id?: string;
	address: string;
	name: string;
	currency: string;
	tax: boolean;
	taxValue: number;
	vat: boolean;
	vatValue: number;
	first: string;
	last: string;
	email: string;
	street: string;
	city: string;
	state: string;
	postal: string;
	phone: string;
	paid: boolean;
	website: string;
	country: string;
	zats: boolean;
	invoices: boolean;
	expiration: string;
	payconf: boolean;
	viewkey: string;
	crmToken: string;
}
