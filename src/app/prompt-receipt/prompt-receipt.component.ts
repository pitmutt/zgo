import { Inject, Component, OnInit} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import { NotifierService } from '../notifier.service';

import { faCopy } from '@fortawesome/free-solid-svg-icons';

var URLSafeBase64 = require('urlsafe-base64');
var Buffer = require('buffer/').Buffer;

@Component({
  selector: 'app-prompt-receipt',
  templateUrl: './prompt-receipt.component.html',
  styleUrls: ['./prompt-receipt.component.css']
})

export class PromptReceiptComponent implements OnInit {
	orderId: string;
	receiptUrl: string;
 
 	// ------------------------------------
	//
	faCopy = faCopy; 
 	// ------------------------------------

	constructor(
			private dialogRef: MatDialogRef<PromptReceiptComponent>,
			@Inject(MAT_DIALOG_DATA) public data: {orderId: string},
			private notifierService : NotifierService ) {
		this.orderId = data.orderId;
		this.receiptUrl = 'https://app.zgo.cash/receipt/'+this.orderId;

	}

	ngOnInit(): void {
	}


	confirm() {
		this.dialogRef.close(true);
	}

	close() {
		this.dialogRef.close(false);
	}

	copyUrl() {
//		console.log("Inside copyUrl()");
		if (navigator.clipboard) {
		};
		try {
			navigator.clipboard.writeText(this.receiptUrl);
			this.notifierService
			    .showNotification("Receipt's URL copied to Clipboard!!","Close",'success');

		} catch (err) {
//			console.error("Error", err);  
			this.notifierService
			    .showNotification("Functionality not available for your browser. Use send button instead.","Close",'error');
		}
	}
}
