export interface Tx {
	_id?: string;
	address: string;
	session: string;
	confirmations: number;
	amount: number;
}
