import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ViewerComponent } from './viewer/viewer.component';
import { ReceiptComponent } from './receipt/receipt.component';
import { LoginComponent } from './login/login.component';
import { BusinessComponent } from './business/business.component';
import { InvoiceComponent } from './invoice/invoice.component';
import { ListOrdersComponent } from './listorders/listorders.component';
import { AuthGuardService } from './auth-guard.service';
import { NodeResolverService } from './node-resolver.service';
import { PmtserviceComponent } from './pmtservice/pmtservice.component';
import { XeroRegComponent } from './xeroreg/xeroreg.component';

const routes: Routes = [
	{ path: '', component: LoginComponent, resolve: { response: NodeResolverService} },
	//{ path: 'create', component: PostCreateComponent, canActivate: [AuthGuardService]},
	{ path: 'shop', component: ViewerComponent, canActivate: [AuthGuardService]},
	{ path: 'orders', component: ListOrdersComponent, canActivate: [AuthGuardService]},
	{ path: 'biz', component: BusinessComponent, canActivate: [AuthGuardService]}, 
	{ path: 'receipt/:orderId', component: ReceiptComponent},
	{ path: 'invoice/:orderId', component: InvoiceComponent},
	{ path: 'pmtservice', component: PmtserviceComponent},
	{ path: 'xeroauth', component: XeroRegComponent},
	{ path: 'login', component: LoginComponent, resolve: { response: NodeResolverService}}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AuthGuardService]
})
export class AppRoutingModule { }
