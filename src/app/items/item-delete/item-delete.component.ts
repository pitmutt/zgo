import { Inject, Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Item } from '../item.model';


@Component({
	selector: 'app-item-delete',
	templateUrl: './item-delete.component.html',
	styleUrls: ['./item-delete.component.css']
})

export class ItemDeleteComponent implements OnInit{
	item: Item;

	constructor(
		private dialogRef: MatDialogRef<ItemDeleteComponent>,
		@Inject(MAT_DIALOG_DATA) public data: Item
	) {
		this.item = data;
	}

	ngOnInit() {
	}

	save() {
		this.dialogRef.close(this.item._id);
	}

	close() {
		this.dialogRef.close();
	}
}
