export interface newLineItem {
    line_id: number;
    qty: number;
    name: string;
    cost: number;
}
