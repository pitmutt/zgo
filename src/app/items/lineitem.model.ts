export interface LineItem {
	qty: number;
	name: string;
	cost: number;
}
