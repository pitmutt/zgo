import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { ReceiptService } from '../receipt.service';
import { Order} from '../order/order.model';
import { Observable }  from 'rxjs';
import { faCheck, faHourglass } from '@fortawesome/free-solid-svg-icons';

import { NotifierService } from '../notifier.service';

var QRCode = require('easyqrcodejs');
var URLSafeBase64 = require('urlsafe-base64');
var Buffer = require('buffer/').Buffer;

@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.css']
})
export class InvoiceComponent implements OnInit {
	faCheck = faCheck;
	faHourglass = faHourglass;
	orderId;
	public orderUpdate: Observable<Order>;
	public nameUpdate: Observable<string>;
	name: string = '';
	error: boolean = false;
	codeString: string = 'Test';

	zcashUrl: SafeUrl = '';


	order:Order = {
		  _id: '',
			address: '',
			session: '',
			timestamp: '',
			closed: false,
			currency: '',
			price: 0,
			total: 0,
			totalZec: 0,
			paid: false,
			externalInvoice: '',
			shortCode: '',
			lines: [
				{
					qty: 1,
					name: '',
					cost:0
				}
			]
		};

	constructor(
		private _ActiveRoute:ActivatedRoute,
		private router: Router,
    private sanitizer: DomSanitizer,
		public receiptService: ReceiptService,
    private notifierService : NotifierService 
	) {
		this.orderId = this._ActiveRoute.snapshot.paramMap.get("orderId");
		this.orderUpdate = receiptService.orderUpdate;
		this.nameUpdate = receiptService.nameUpdate;
		receiptService.getOrderById(this.orderId!).subscribe(response => {
			if (response.status == 200){
				this.error = false;
				this.codeString = `zcash:${response.body!.order.address}?amount=${response.body!.order.totalZec.toFixed(8)}&memo=${URLSafeBase64.encode(Buffer.from('ZGo Order::'.concat(this.orderId!)))}`; 
				var qrcode = new QRCode(document.getElementById("payment-qr"), {
		        text: this.codeString,
						logo: "/assets/zcash.png",
						width: 180,
						height: 180,
						logoWidth: 50,
						logoHeight: 50,
		        correctLevel: QRCode.CorrectLevel.H
				});
				this.error = false;
			} else {
				this.error = true;
				this.codeString = 'Test';
			}
		});
		this.orderUpdate.subscribe(order => {
			this.order = order;
      this.codeString = `zcash:${this.order.address}?amount=${this.order.totalZec.toFixed(8)}&memo=${URLSafeBase64.encode(Buffer.from('ZGo Order::'.concat(this.orderId!)))}`; 
      this.zcashUrl = this.sanitizer.bypassSecurityTrustUrl(this.codeString);

		});
		this.nameUpdate.subscribe(name => {
			this.name = name;
		});
	}

	ngOnInit(): void {
	}

	getIconStyle(order : Order) {
		if( order.paid )
		  return "font-size: 14px; color: #72cc50; margin-bottom: -2px;";
		return "color: #FB4F14; margin-bottom: -2px; cursor: pointer;";

	}

   copyAddress() {
      if (!navigator.clipboard) {
//       alert("Copy functionality not supported");
         this.notifierService
             .showNotification("Copy functionality not supported","Close","error");
      }
      try {
         navigator.clipboard.writeText(this.order.address);
      } catch (err) {
         this.notifierService
             .showNotification("Error copying address","Close","error");
//       console.error("Error", err);
      }
   }
   copyAmount() {
      if (!navigator.clipboard) {
//       alert("Copy functionality not supported");
         this.notifierService
             .showNotification("Copy functionality not supported","Close","error");
      }
      try {
         navigator.clipboard.writeText(this.order.totalZec.toString());
      } catch (err) {
         this.notifierService
             .showNotification("Error while copying ammount","Close","error");
//       console.error("Error", err);
      }
   }

   copyMemo() {
      if (!navigator.clipboard) {
//       alert("Copy functionality not supported");
         this.notifierService
             .showNotification("Copy functionality not supported","Close","error");
      }
      try {
         navigator.clipboard.writeText("ZGo Order::" + this.order._id);
      } catch (err) {
         this.notifierService
             .showNotification("Error while copying Memo","Close","error");
//       console.error("Error", err);
      }
   }

}
