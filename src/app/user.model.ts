export interface User {
	_id?: string;
	address: string;
	session: string;
	blocktime: number;
	pin: string;
	validated: boolean;
}
